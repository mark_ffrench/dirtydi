﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public static class DirtyDI
{
    static DirtyDI()
    {
        EditorApplication.projectChanged += AutoInject;
        EditorApplication.hierarchyChanged += AutoInject;
    }

    private const string k_toggleAutoInjectMenuName = "DirtyDI/AutoInject";
    private const string k_contextInjectMenuName = "CONTEXT/MonoBehaviour/Inject";
    private const string k_sceneInjectMenuName = "DirtyDI/Inject Scene";
    private const string k_projectInjectMenuName = "DirtyDI/Inject Project";
    private static bool autoInjectEnabled = true;
    
    [MenuItem(k_toggleAutoInjectMenuName)]
    private static void ToggleAutoInject()
    {
        autoInjectEnabled = !autoInjectEnabled;
        Menu.SetChecked(k_toggleAutoInjectMenuName, autoInjectEnabled);
        EditorPrefs.SetBool(k_toggleAutoInjectMenuName, autoInjectEnabled);
        
        if(autoInjectEnabled)
            AutoInject();
    }

    private static void AutoInject()
    {
        if(!autoInjectEnabled)
            return;
        
        InjectProject();
        InjectScene();
    }
    
    [MenuItem(k_contextInjectMenuName)]
    private static void InjectScript(MenuCommand command)
    {
        InjectObject(command.context);
    }

    [MenuItem(k_sceneInjectMenuName)]
    public static void InjectScene()
    {
        MonoBehaviour[] sceneActive = GameObject.FindObjectsOfType<MonoBehaviour>();

        foreach (MonoBehaviour mono in sceneActive)
        {
            InjectObject(mono);
        }
    }

    [MenuItem(k_projectInjectMenuName)]
    public static void InjectProject()
    {
        GameObject[] prefabs = GetAllInstances<GameObject>();

        foreach (GameObject go in prefabs)
        {
            foreach (MonoBehaviour mono in go.GetComponents<MonoBehaviour>())
            {
                InjectObject(mono);
            }
        }

        ScriptableObject[] scriptableObjects = GetAllInstances<ScriptableObject>();

        foreach (ScriptableObject so in scriptableObjects)
        {
            InjectObject(so);
        }
    }

    public static void InjectObject(UnityEngine.Object obj)
    {
        Type monoType = obj.GetType();

        // Retreive the fields from the mono instance
        FieldInfo[] objectFields = monoType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);

        // search all fields and find the attribute [Position]
        foreach (FieldInfo t in objectFields)
        {
            InjectNamedAttribute attribute = Attribute.GetCustomAttribute(t, typeof(InjectNamedAttribute)) as InjectNamedAttribute;
            InjectLabelAttribute labelAttribute = Attribute.GetCustomAttribute(t, typeof(InjectLabelAttribute)) as InjectLabelAttribute;
            InjectChildAttribute injectChild = Attribute.GetCustomAttribute(t, typeof(InjectChildAttribute)) as InjectChildAttribute;
            InjectComponentAttribute injectComponent = Attribute.GetCustomAttribute(t, typeof(InjectComponentAttribute)) as InjectComponentAttribute;

            if (attribute != null)
            {
                t.SetValue(obj, Get(attribute.Id, t.FieldType));
                EditorUtility.SetDirty(obj);
            }

            if (labelAttribute != null)
            {
                if (string.IsNullOrEmpty(labelAttribute.Label))
                {
                    //match labels on the object being inserted
                    string[] labels = AssetDatabase.GetLabels(obj);
                    if (labels == null || labels.Length == 0)
                    {
                        Debug.LogErrorFormat("Trying to Inject a {0} which has the same labels as \"{1}\", but \"{1}\" has no labels on it.", t.FieldType, obj.name);
                        continue;
                    }

                    t.SetValue(obj, GetLabelled(labels[0], t.FieldType));
                }
                else
                {
                    t.SetValue(obj, GetLabelled(labelAttribute.Label, t.FieldType));
                }

                EditorUtility.SetDirty(obj);
            }
            

            if (injectComponent != null)
            {
                Component[] components = ((MonoBehaviour) obj).GetComponents(t.FieldType);

                if (components.Length == 1)
                {
                    t.SetValue(obj, components[0]);
                }
                else
                {
                    t.SetValue(obj, null);
                }
                
                EditorUtility.SetDirty(obj);
            }
            
            if (injectChild != null)
            {
                Component[] components = ((MonoBehaviour) obj).GetComponentsInChildren(t.FieldType);
                
                if (components.Length == 1)
                {
                    t.SetValue(obj, components[0]);
                }
                else
                {
                    t.SetValue(obj, null);
                }
                
                EditorUtility.SetDirty(obj);
            }
        }
    }

    private static T[] GetAllInstances<T>() where T : UnityEngine.Object
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name); //FindAssets uses tags check documentation for more info
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;
    }

    private static UnityEngine.Object[] GetNamedInstances(Type type, string name)
    {
        string[] guids = AssetDatabase.FindAssets("t: " + type.Name + " " + name);
        UnityEngine.Object[] a = new UnityEngine.Object[guids.Length];
        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
        }

        return a;
    }

    private static UnityEngine.Object[] GetLabelledInstances(Type type, string label)
    {
        string[] guids = AssetDatabase.FindAssets("t: " + type.Name + " l:" + label);
        UnityEngine.Object[] a = new UnityEngine.Object[guids.Length];
        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
        }

        return a;
    }

    private static MonoBehaviour GetMonoBehaviourOnLabelledPrefab(Type t, string label)
    {
        string[] guids = AssetDatabase.FindAssets("t: GameObject l:" + label); 
        
        GameObject[] a = new GameObject[guids.Length];

        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            MonoBehaviour m = (MonoBehaviour) a[i].GetComponent(t);
            if (m != null)
                return m;
        }

        Debug.LogErrorFormat("Couldn't find prefab labelled {0} with a {1} on it", label, t);
        return null;
    }

    private static MonoBehaviour GetMonoBehaviourOnPrefab(Type t, string name)
    {
        string[] guids = AssetDatabase.FindAssets("t: GameObject " + name);
        GameObject[] a = new GameObject[guids.Length];

        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            MonoBehaviour m = (MonoBehaviour) a[i].GetComponent(t);
            if (m != null)
                return m;
        }

        Debug.LogErrorFormat("Couldn't find prefab called {0} with a {1} on it", name, t);
        return null;
    }

    public static GameObject Instantiate(GameObject prefab)
    {
        GameObject instance = GameObject.Instantiate(prefab);

        foreach (MonoBehaviour mono in instance.GetComponents<MonoBehaviour>())
        {
            Type monoType = mono.GetType();

            // Retrieve the fields from the mono instance
            FieldInfo[] objectFields = monoType.GetFields(BindingFlags.Instance | BindingFlags.Public);

            // search all fields and find the attribute [Position]
            for (int i = 0; i < objectFields.Length; i++)
            {
                InjectNamedAttribute attribute =
                    Attribute.GetCustomAttribute(objectFields[i], typeof(InjectNamedAttribute)) as InjectNamedAttribute;

                if (attribute != null)
                {
                    objectFields[i].SetValue(mono, Get(attribute.Id, objectFields[i].FieldType));
                }
            }
        }

        return instance;
    }

    public static T Instantiate<T>(T prefab) where T : MonoBehaviour
    {
        T instance = GameObject.Instantiate<T>(prefab);

        Type monoType = instance.GetType();

        // Retrieve the fields from the mono instance
        FieldInfo[] objectFields = monoType.GetFields(BindingFlags.Instance | BindingFlags.Public);

        // search all fields and find the attribute [Position]
        for (int i = 0; i < objectFields.Length; i++)
        {
            InjectNamedAttribute attribute = Attribute.GetCustomAttribute(objectFields[i], typeof(InjectNamedAttribute)) as InjectNamedAttribute;

            if (attribute != null)
            {
                objectFields[i].SetValue(instance, Get(attribute.Id, objectFields[i].FieldType));
            }
        }

        return instance;
    }

    public static UnityEngine.Object GetLabelled(string label, Type type)
    {
        if (type.IsSubclassOf(typeof(MonoBehaviour)))
        {
            return GetMonoBehaviourOnLabelledPrefab(type, label);
        }
        else //(type.IsSubclassOf(typeof(ScriptableObject)) || type == typeof(GameObject))
        {
            UnityEngine.Object[] objs = GetLabelledInstances(type, label);

            if (objs.Length == 0)
            {
                Debug.LogFormat("Couldn't find any {0} objects labelled {1} in the project", type, label);
                return null;
            }
            else if (objs.Length > 1)
            {
                Debug.LogWarningFormat("Found {0} {1} labelled {2}, injecting the first one found", objs.Length, type,
                    label);
            }

            return objs.FirstOrDefault();
        }
    }

    public static UnityEngine.Object Get(string id, Type type)
    {
        if (type.IsSubclassOf(typeof(MonoBehaviour)))
        {
            if (string.IsNullOrEmpty(id))
            {
                Debug.LogFormat("Injecting unnamed prefabs is not supported ({0})", type);
                return null;
            }

            return GetMonoBehaviourOnPrefab(type, id);
        }
        else //if (type.IsSubclassOf(typeof(ScriptableObject)) || type == typeof(GameObject))
        {
            UnityEngine.Object[] objs = GetNamedInstances(type, id);

            if (objs.Length == 0)
            {
                Debug.LogFormat("Couldn't find any {0} objects in the project", type);
                return null;
            }

            foreach (UnityEngine.Object o in objs)
            {
                if (o == null)
                {
                    Debug.LogWarning("Weird");
                    continue;
                }

                if (o.name == id || string.IsNullOrEmpty(id))
                    return o;
            }

            Debug.LogFormat("No {0} found called {1}", type, id);
            return null;
        }
    }
}