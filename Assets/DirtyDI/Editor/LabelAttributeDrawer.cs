﻿using System;
using UnityEditor;
using UnityEngine;

internal static class InjectedPropertyDrawer
{
    public static void DrawInjectedPropertyField(Rect position, SerializedProperty property, GUIContent label)
    {
        Color normal = GUI.color;
        GUI.enabled = false;
        GUI.color = property.objectReferenceValue != null ? Color.green : Color.red;
        EditorGUI.PropertyField(position, property, label);
        GUI.enabled = true;
        GUI.color = normal;
    }
}

[CustomPropertyDrawer(typeof(InjectSingleAttribute))]
public class InjectSingleAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InjectedPropertyDrawer.DrawInjectedPropertyField(position, property, label);
    }
}

[CustomPropertyDrawer(typeof(InjectNamedAttribute))]
public class InjectNamedAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InjectedPropertyDrawer.DrawInjectedPropertyField(position, property, label);
    }
}


[CustomPropertyDrawer(typeof(InjectLabelAttribute))]
public class InjectLabelAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InjectedPropertyDrawer.DrawInjectedPropertyField(position, property, label);
    }
}


[CustomPropertyDrawer(typeof(InjectComponentAttribute))]
public class InjectComponentAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InjectedPropertyDrawer.DrawInjectedPropertyField(position, property, label);
    }
}

[CustomPropertyDrawer(typeof(InjectChildAttribute))]
public class InjectChildAttributeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        InjectedPropertyDrawer.DrawInjectedPropertyField(position, property, label);
    }
}