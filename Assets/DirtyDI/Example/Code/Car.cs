﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Car : MonoBehaviour
{
    [SerializeField, InjectNamed("WheelPrefab")] private GameObject wheelPrefab;
    
    [SerializeField, InjectLabel] private AudioClip hornSound;
    [SerializeField, InjectLabel] private DriverBrain carBrain;
    
    private void Start()
    {
        GameObject wheel1 = Instantiate(wheelPrefab, transform);
        wheel1.transform.localPosition = new Vector3(0.5f, -0.4f, -0.4f);
        
        GameObject wheel2 = Instantiate(wheelPrefab, transform);
        wheel2.transform.localPosition = new Vector3(-0.5f, -0.4f, -0.4f);
    }

    private void OnMouseUpAsButton()
    {
        GetComponent<AudioSource>().PlayOneShot(hornSound);
        carBrain.SayHello();
    }
}