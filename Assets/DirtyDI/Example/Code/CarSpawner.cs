﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
	[SerializeField, InjectLabel("Player")] 
	private Car PlayerCar;
	
	[SerializeField, InjectLabel("Enemy")] 
	private Car EnemyCar;
	
	private void Start()
	{
		Car playerInstance = Instantiate(PlayerCar);
		playerInstance.transform.position = new Vector3(2f, 0, 0);
		
		Car enemyInstance = Instantiate(EnemyCar);
		enemyInstance.transform.position = new Vector3(-2f, 0, 0);
	}
}
