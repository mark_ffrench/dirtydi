﻿using UnityEngine;

[CreateAssetMenu]
public class DriverBrain : ScriptableObject
{
    [SerializeField] private string FirstName;
    
    public void SayHello()
    {
        Debug.Log("Hi, my name is "+FirstName);
    }
}