﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyButton : MonoBehaviour
{
    [InjectComponent]
    public Button button;
    
    [InjectChild]
    public Text text;
}
