﻿using System;
using System.Reflection;
using UnityEngine;

public class InjectSingleAttribute : PropertyAttribute
{

}


public class InjectNamedAttribute : PropertyAttribute
{
    public string Id { get; private set; }

    public InjectNamedAttribute()
    {
        Id = "";
    }
    
    public InjectNamedAttribute(string id)
    {
        Id = id;
    }
}

public class InjectLabelAttribute : PropertyAttribute
{
    public string Label { get; private set; }

    public InjectLabelAttribute()
    {
        Label = "";
    }
    
    public InjectLabelAttribute(string label)
    {
        Label = label;
    }
}

public class InjectComponentAttribute : PropertyAttribute
{
    
}

public class InjectChildAttribute : PropertyAttribute
{
    
}