# DirtyDI

### DirtyDI automatically finds empty inspector fields with the [Inject] attribute and assigns a matching asset from your project.

## Introduction

DirtyDI is a quick and **dirty Dependency Injection** system for Unity.

Dragging and dropping an asset into an inspector field is one of the easiest ways to inject a dependency into a Unity Object:

* No expensive run-time searching for gameobjects in your scene, or searching for assets in your project
* No need to tightly couple your code to a concrete instance of a class, such as a singleton
* No need for big, complicated dependency injection frameworks.

However, maintaining these dependencies by manually assigning assets in the inspector panel can be time-consuming once your project grows. Worse, Unity's serialization system is prone to breaking, should you try to rename classes and variables, leaving you having to reassign broken references.

## Examples

### InjectComponent

Injects a reference to a monobehaviour on the same object, equivalent to calling GetComponent in Awake()

```csharp
	[InjectComponent]
	public Button button;
```

------

### InjectChild

Injects a reference to a monobehaviour in a child object, equivalent to calling GetComponentInChildren in Awake()

```csharp
	[InjectComponent]
	public Button button;
```

------


### InjectSingle

Lets say we have a prefab or scene object with the script ```Car``` on it: 
```csharp
public class Car : MonoBehaviour
{
	public Driver driver;
}
```

We want to make sure that prefab always has a reference to the ```Driver``` asset, which is a ScriptableObject in our project
```csharp
public class Driver : ScriptableObject
{
	public string FirstName;
}
```

To avoid having to manually assign the ```Driver``` asset, we can add an attribute to our ```driver``` variable: 

```csharp
	[InjectSingle]
	public Driver driver;
```
> This searches the project for a single asset of the **type** `Driver` and automatically assigns it in the inspector.

If we now look at our prefab we should see that the reference has been magically assigned without our input:

![An injected dependency](http://www.markffrench.com/wp-content/uploads/2018/06/assigned_reference.png)

We no longer have to worry about manually updating every prefab with the ```Car``` script on it. In vanilla Unity, if we deleted the ```Driver``` ScriptableObject, our car prefabs would silently have become broken. With DirtyDI, the editor will warn us that it wasn't able to fill the ```Driver``` field.

------

### InjectNamed

If we have more than one Driver asset in our project, we can specify which one we want by explicitly naming the object we want to inject.

```csharp
	[InjectNamed("PlayerDriver")]
	public Driver driver;
```
> This searches the project for a `Driver` asset with the **name** `PlayerDriver`

------

### InjectLabel

DirtyDI also supports injecting assets that are tagged using Unity's asset labelling system. You can add a label to an asset by clicking the blue luggage tag in the bottom right of the asset preview:

![How to add asset labels](http://www.markffrench.com/wp-content/uploads/2018/06/add_label.png)

If the object we're injecting into has a label on it, then ```InjectLabel``` will try to match that when searching for the asset to be injected.

For example, a Car labelled `Player` will only be injected with assets that are also labelled `Player`

```csharp
	[InjectLabel]
	public Driver driver;
```
>This searches the project for a `Driver` ScriptableObject with the **label** `Player`

### Combined Labels

We can combine labels on the `Car` prefab with labels explicitly defined in the attribute:

```csharp
	[InjectLabel("Horn"]
	public AudioClip hornSound;
```
> This searches the project for an `AudioClip` with the **labels** `Horn` *and* `Player`

![Injected horn asset](http://www.markffrench.com/wp-content/uploads/2018/06/combined_labels.png)


If we instead took our `Car` prefab and added the label `Enemy`, it would be assigned the audio clip labelled `Horn` and `Enemy`

![Injected horn asset](http://www.markffrench.com/wp-content/uploads/2018/06/combined_labels_enemy.png)

## How does it work?

DirtyDI uses AssetDatabase.FindAsset() to find the dependencies you want to inject. The results are identical to those found when searching in the Project. For example:

```csharp
	[InjectLabel("Player"]
	public Driver driver;
```

would translate to the search string:

![Asset Database Search](http://www.markffrench.com/wp-content/uploads/2018/06/asset_database_search.png)

Where:
```
	n: name
	t: type
	l: label
```
If DirtyDI doesn't find **exactly one** result that matches your criteria, you'll get a warning in the log.

DirtyDI will inject dependencies into objects in your current scene. However, Unity doesn't support labelling scene objects. Therefore you can't combine hardcoded labels with labels applied to scene objects.



By default DirtyDI auto injects all dependencies whenever there is a change to the project. In big projects with a lot of injected references and a lot of assets this will probably be slow, so you may want to disable autoinjection by toggling the menu option DirtyDI->AutoInject.

## Limitations

* Injection is not possible at run-time, DirtyDI simply wires things together in the editor
* DirtyDI can inject into objects in your scene, but scene objects can't be labelled - so asset+hardcoded label combinations are not possible
* Doesn't currently support injecting sprites, or other nested assets
* Not particularly well optimised yet

------

[![Michael Jackson - Dirty Diana](https://img.youtube.com/vi/yUi_S6YWjZw/0.jpg)](http://www.youtube.com/watch?v=yUi_S6YWjZw)

